package dam.m3.uf5.exceptions;

public class ProvesCalculadoraEnters {
    public static void main(String[] args) throws InfinitException, IndeterminatException {
        CalculadoraEnters ce = new CalculadoraEnters();
        System.out.println(ce.sumar(2,2));
        System.out.println(ce.restar(2,2));
        System.out.println(ce.multiplicar(2,2));
        System.out.println(ce.dividir(0,0));
        System.out.println(ce.dividir(4,0));
        System.out.println(ce.dividir(2,2));
    }
}
