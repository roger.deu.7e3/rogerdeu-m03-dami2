package dam.m3.uf5.exceptions;

import java.util.Scanner;

public class ExemplesCodi {
    public static void main(String[] args) {
        int[] vector = new int[3];
        String paraula = "abab";

        //1-System.out.println(vector[6]); //ArrayIndexOutOfBounds perquè intento accedir a una posició del vector que no existeix i el sobrepasso.
                                        //Es pot evitar fent un vector.length()-1, ja que així controlo no excedir el màxim.
        //2-System.out.println(paraula.charAt(5)); //StringIndexOutOfBoundsException perque intento accedir a un caracter que sobrepasa la string.
                                                //Es pot evitar fent un paraula.length() o cambiat el valor a un de més petit.
        //3-Object i = Integer.valueOf(42); String s = (String)i; //ClassCastException, dona un error perque Integer no es una sublasse de string.
                                                                //Es pot cambiar fent un String.valueOf(i), ja que un cast no es pot realitzar directament.
        //4-System.out.println(3/0); //ArithmeticException perquè no es valid aritmeticament fer una divisió entre zero.
                                //Aquí el que s'ha de fer es cambiar l'operació per una de valida.
        //5-int[] arrayNegatiu = new int[-4]; //NegativeArraySizeException ja que li assignem un tamany negatiu.
                                            //El que hem de fer es cambiar el signe del numero de l'array
        //6-String numero = "ewfwef";
        //int numrocast = Integer.parseInt(numero); //FormatNumberException perque no for ser un int una cadena de text.
                                                    //Això s'ha de cambiar treient la variable ja que hi ha alguna cosa que no s'hauria fet bé.


        /*if (paraula.contains("-")) {  //7- Aqui salta la excepció perquè l'argument no conte el que es busca, s'ha de cambiar l'argument.
            System.out.println("Contiene -");
        } else {
            throw new IllegalArgumentException("String " + paraula + " no contiene -");
        }*/
        //Integer hola=null;  //8-NullPointerExcepcion perquè s'intenta obtindre informació d'una variable null, això s'arregla cambiant la variable.
        //System.out.println(hola.getClass());

        //Scanner lector = new Scanner(System.in); //8- Si aqui introdueixes una cadena de text dona l'error, això es corregeix fent un hasnextdouble per a validar.
        //lector.nextDouble();

       /* try {
            Class.forName("com.journaldev.MyInvisibleClass");

            ClassLoader.getSystemClassLoader().loadClass("com.journaldev.MyInvisibleClass");

            ClassLoader.getPlatformClassLoader().loadClass("com.journaldev.MyInvisibleClass");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }*/ //9- ClassNotFoundException perquè intentes accedir a una classe que no existeix.

        //----------- Exercici 2--------------

        //No such method error: Aquest error succeix quan en runtime intentes cridar a un metode que encara no existeix. Es pot evitar mirant els metodes que s'han d'executar,
        //que estiguin correctament per a ser cridats.

        //NoClassDefFoundError: Aquest error passa quan s'esta executant el programa i s'intenta utilitzar una classe no definida. Es poden evitar declarant les classes
        //abans de treballar amb elles.

    }

}
