package dam.m3.uf5.exceptions;

import java.io.File;
import java.util.Scanner;

public class LlegirFitxer {
    static int[] sumarInts = new int[1];
    public static void main(String[] args){

        File f = new File("hola.txt");

        try {
            if (f.exists()) {
                System.out.println("El fitxer existeix");
            } else {
                throw new FitxerNoExisteixException();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            f.createNewFile();
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            Scanner lector = new Scanner(f);
            int contadorEnters=0,contadorNoEnters=0, numeroEnter = 0;
            while (lector.hasNext()){
                String numero = String.valueOf(lector.next());
                if (cambiarInt(numero)) {
                    contadorEnters++;
                } else {
                    contadorNoEnters++;
                    System.out.println("Element no enter: "+numero);
                }
            }
            System.out.println("Contador de tipus de dades no int: "+contadorNoEnters);
            System.out.println("Contador de dades de tipus int: "+contadorEnters+" i aquesta es la seva suma: "+sumarInts[0]);
        } catch (Exception e) {

        }

    }
    public static boolean cambiarInt(String paraula){
        int numeroEnter = 0;
        try {
            numeroEnter = Integer.valueOf(paraula);
            sumarInts[0] = numeroEnter+sumarInts[0];
            return true;
        } catch (Exception e){
            return false;
        }
    }
}
