package dam.m3.uf5.exceptions;

public class CalculadoraEnters {
    public static int sumar(int a, int b){
        return a+b;
    }
    public static int restar(int a, int b){
        return a-b;
    }
    public static int multiplicar(int a, int b){
        return a*b;
    }
    public static int dividir(int a, int b) throws IndeterminatException, InfinitException {
        int resultat=0;
            try {
                if (b==0 && a==0) {
                    throw new IndeterminatException();
                } else if (b==0){
                    throw new InfinitException();
                }
                resultat = a/b;
            } catch (Exception e) {
                e.printStackTrace();
            }
            return resultat;
    }
}
