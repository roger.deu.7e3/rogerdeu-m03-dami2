package dam.m3.uf5.maps;
import java.util.*;

public class HashMapp {
    /**
     * 1) Canviar HashMap per LinkedHashMap i TreeMap
     * 2) Introduir clau classe Etiqueta int id String descripcio
     * @author Roger Deu Martí
     */
    public static void main(String[] args) {
        Etiqueta et = new Etiqueta(1,"h");
        Etiqueta et2 = new Etiqueta(5,"ewg");
        Etiqueta et3 = new Etiqueta(3,"r");
        Etiqueta et4 = new Etiqueta(4,"3");
        Etiqueta et5 = new Etiqueta(2,"a");

        HashMap<String,Etiqueta> map = new HashMap();
        /*map.put("34343434","rgregg");
        map.put("342543655","rgg");
        map.put("124324","ekise");
        map.put("97948609","zowf");
        map.put("1111","aiwdjiu");*/

        map.put("34343434",et);
        map.put("342543655",et2);
        map.put("124324",et3);
        map.put("97948609",et4);
        map.put("1111",et5);

        Set<String> s = map.keySet();

        for (String clau: s) {
            System.out.println(clau);
        }

        Collection<Etiqueta> col = map.values();

        for (Etiqueta value: col) {
            System.out.println(value);
        }

        Set<Map.Entry<String,Etiqueta>> entrades = map.entrySet();
        for (Map.Entry<String,Etiqueta> entrada : entrades) {
            System.out.println(entrada);
        }

        /*LinkedHashMap<String,String> lmap = new LinkedHashMap<>();
        lmap.put("34343434","rgregg");
        lmap.put("342543655","rgg");
        lmap.put("124324","ekise");
        lmap.put("97948609","zowf");
        lmap.put("1111","aiwdjiu");

        Set<String> valor = lmap.keySet();
        Collection<String> clau = lmap.values();
        Set<Map.Entry<String, String>> conjunt = lmap.entrySet();

        for (Map.Entry<String,String> m:conjunt){
            System.out.println(m);
        }

        for (String s:valor){
            System.out.println(s);
        }

        for (String s:clau){
            System.out.println(s);
        }*/

        /*TreeMap<String,String> arbre = new TreeMap<>();
        arbre.put("34343434","rgregg");
        arbre.put("342543655","rgg");
        arbre.put("124324","ekise");
        arbre.put("97948609","zowf");
        arbre.put("1111","aiwdjiu");

        Set<String> clau = arbre.keySet();
        Collection<String> valor = arbre.values();
        Set<Map.Entry<String, String>> conjunt = arbre.entrySet();

        for (Map.Entry<String,String> conj:conjunt) {
            System.out.println(conj);
        }

        for (String claus : clau){
            System.out.println(claus);
        }

        for (String valors : valor) {
            System.out.println(valors);
        }*/
    }

}
