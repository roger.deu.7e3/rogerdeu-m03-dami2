package dam.m3.uf5.maps;

public class Etiqueta {
    private int id;
    private String descripcio;

    public Etiqueta(int id, String descripcio) {
        this.id = id;
        this.descripcio = descripcio;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    @Override
    public String toString() {
        return "Etiqueta{" +
                "id=" + id +
                ", descripcio='" + descripcio + '\'' +
                '}';
    }
}
