package dam.m3.uf5.funcionals;

import java.util.ArrayList;
import java.util.List;

public class ProvaCamisa {
    public static void main(String[] args) {
        ArrayList<Camisa> array = new ArrayList<>();
        array.add(new Camisa("2","XL","blau"));
        array.add(new Camisa("1","L","verd"));
        array.add(new Camisa("6","XL","blau"));
        array.add(new Camisa("4","M","blau"));
        array.add(new Camisa("3","XL","vermell"));

        System.out.println("-------------Filtrar per talla xl-------------");

        filtrarCamisa(array, new CaracteristicaCamisa() {
            @Override
            public boolean test(Camisa c) {
                return c.getTalla().equalsIgnoreCase("xl");
            }
        });

        System.out.println("-------------Filtrar per color vermell-------------");

        filtrarCamisa(array, new CaracteristicaCamisa() {
            @Override
            public boolean test(Camisa c) {
                return c.getColor().equalsIgnoreCase("vermell");
            }
        });

        System.out.println("-------------Filtrar per talla m i color blau-------------");

        filtrarCamisa(array, new CaracteristicaCamisa() {
            @Override
            public boolean test(Camisa c) {
                return c.getColor().equalsIgnoreCase("blau") && c.getTalla().equalsIgnoreCase("m");
            }
        });

        filtrarCamisa(array, (Camisa c)->c.getTalla().equalsIgnoreCase("XL"));

        filtrarCamisa(array, (Camisa c)->c.getTalla().equalsIgnoreCase("m")&&c.getColor().equalsIgnoreCase("blau"));

        filtrarCamisa(array, (Camisa c)->c.getColor().equalsIgnoreCase("vermell"));

        //array.forEach((x)-> System.out.println(x.toString()));
    }
    private static void filtrarCamisa(List<Camisa> array, CaracteristicaCamisa c){
        for (Camisa cam: array) {
            if (c.test(cam)) {
                System.out.println(cam);
            }
        }
    }

    @FunctionalInterface
    interface CaracteristicaCamisa{
        boolean test(Camisa c);
    }
}
