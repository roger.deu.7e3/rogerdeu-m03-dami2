package dam.m3.uf5.iteradors;

public class Emissora {
    private double freqüencia;
    private String especialitat, nom;

    public Emissora(double freqüencia, String especialitat, String nom) {
        this.freqüencia = freqüencia;
        this.especialitat = especialitat;
        this.nom = nom;
    }

    @Override
    public String toString() {
        return "Emissora{" +
                "freqüencia=" + freqüencia +
                ", especialitat='" + especialitat + '\'' +
                ", nom='" + nom + '\'' +
                '}';
    }

    public double getFreqüencia() {
        return freqüencia;
    }

    public void setFreqüencia(double freqüencia) {
        this.freqüencia = freqüencia;
    }

    public String getEspecialitat() {
        return especialitat;
    }

    public void setEspecialitat(String especialitat) {
        this.especialitat = especialitat;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }
}
