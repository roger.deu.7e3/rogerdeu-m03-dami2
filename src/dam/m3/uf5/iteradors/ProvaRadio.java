package dam.m3.uf5.iteradors;

public class ProvaRadio {
    public static void main(String[] args) {
        Radio r = new Radio();
        r.afegirEmissora(new Emissora(50.2,"classica","ClassicaFM"));
        r.afegirEmissora(new Emissora(88.4,"Noticies","RAC1"));
        r.afegirEmissora(new Emissora(101.2,"Noticies","SER"));
        r.afegirEmissora(new Emissora(45.7,"rock","RockFM"));
        r.afegirEmissora(new Emissora(90.8,"classica","Class"));

        for (Emissora e: r) {
            System.out.println(e);
        }
    }
}
