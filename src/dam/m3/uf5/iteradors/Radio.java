package dam.m3.uf5.iteradors;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Radio implements Iterable<Emissora>{
    List<Emissora> radio = new ArrayList();

    public void afegirEmissora(Emissora e) {
        radio.add(e);
    }

    @Override
    public Iterator<Emissora> iterator() {
        return radio.iterator();
    }
}