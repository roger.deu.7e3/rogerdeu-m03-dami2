package dam.m3.uf5.iteradors2;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class Radio implements Iterable<Emissora>{
    List<Emissora> radio = new ArrayList();

    public void afegirEmissora(Emissora e) {
        radio.add(e);
    }

    @Override
    public Iterator<Emissora> iterator() {
        return new iteradorClassica(radio);
    }

    class iteradorClassica implements Iterator<Emissora>{
        private List<Emissora> emissores;
        private int actual = 0;

        public iteradorClassica(List<Emissora> e) {
            emissores = e;
        }

        @Override
        public boolean hasNext() {
            boolean hiha = false;
            int i = actual;
            while (i<emissores.size() && !hiha) {
                if (emissores.get(i).getEspecialitat().equalsIgnoreCase("classica")){
                    hiha = true;
                } else {
                    i++;
                }
            }
            return hiha;
        }

        @Override
        public Emissora next() {
            Emissora em = emissores.get(actual++);
            while (!em.getEspecialitat().equalsIgnoreCase("classica")){
                em = emissores.get(actual++);
            }
            return em;
        }
    }
}