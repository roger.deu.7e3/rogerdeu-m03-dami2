package dam.m3.uf5.primerrepas;

public class Persona {
    private String nom;
    private int edat;

    public Persona(String nom, int edat) {
        this.nom = nom;
        this.edat = edat;
    }

    @Override
    public String toString() {
        return "dam.m3.uf5.primerrepas.Persona{" +
                "nom='" + nom + '\'' +
                ", edat=" + edat +
                '}';
    }

    public String getNom() {
        return nom;
    }

    public int getEdat() {
        return edat;
    }

    public static int comparaPerNom(Persona p1, Persona p2) {
        return p1.getNom().compareToIgnoreCase(p2.getNom());
    }
}
