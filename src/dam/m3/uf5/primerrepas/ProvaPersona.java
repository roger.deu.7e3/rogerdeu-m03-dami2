package dam.m3.uf5.primerrepas;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class ProvaPersona {
    public static void main(String[] args) {
        Persona[] vector = new Persona[5];
        vector[0] = new Persona("Roger", 19);
        vector[1] = new Persona("abb", 12);
        vector[2] = new Persona("keke", 87);
        vector[3] = new Persona("Zefe", 22);
        vector[4] = new Persona("Akefk", 8);
        ArrayList<Persona> llista;

        llista = new ArrayList<Persona>(Arrays.asList(vector[0], vector[1], vector[2], vector[3], vector[4]));
        System.out.println("Llista original\n");
        System.out.println(llista);

        Collections.sort(llista, new ComparatorPerNom());    //Comparator amb una classe auxiliar ordenant per nom
        System.out.println("\nLlista classe auxiliar per nom\n");
        System.out.println(llista);

        Collections.sort(llista, new ComparatorPerEdat());   //Comparator amb una classe auxiliar ordenant per edat
        System.out.println("\nLlista classe auxiliar per edat\n");
        System.out.println(llista);

        System.out.println("\nLlista classe interna per nom\n");
        Collections.sort(llista, new Comparator<Persona>() {    //Comparator amb una classe interna ordenant per nom
            @Override
            public int compare(Persona persona, Persona t1) {
                return persona.getNom().compareToIgnoreCase(t1.getNom());
            }
        });
        System.out.println(llista);

        System.out.println("\nLlista classe interna per edat\n");
        Collections.sort(llista, new Comparator<Persona>() {    //Comparator amb una classe interna ordenant per edat
            @Override
            public int compare(Persona persona, Persona t1) {
                return persona.getEdat() - t1.getEdat();
            }
        });
        System.out.println(llista);

        Collections.sort(llista, (p1, p2) -> p1.getNom().compareToIgnoreCase(p2.getNom())); //Comparator amb lambda ordenant per nom
        System.out.println("\nLlista lambda per nom\n");
        llista.forEach(System.out::println);

        System.out.println("\nLlista lambda per edat\n");
        Collections.sort(llista, (p1, p2) -> p1.getEdat() - p2.getEdat()); //Comparator amb lambda ordenant per edat
        llista.forEach(System.out::println);

        llista.sort(Persona::comparaPerNom);
        System.out.println("\nComparant per nom exercici 2\n"); //Exercici 2 ordenació
        System.out.println(llista);
    }

}

class ComparatorPerNom implements Comparator<Persona> {

    @Override
    public int compare(Persona persona, Persona t1) {
        return persona.getNom().compareToIgnoreCase(t1.getNom());
    }
}

class ComparatorPerEdat implements Comparator<Persona> {

    @Override
    public int compare(Persona persona, Persona t1) {
        return persona.getEdat() - t1.getEdat();
    }
}

