package dam.m3.uf5.poo;

public class Alumne {
    private String nom, cognoms;
    private int edat;
    private boolean sapjava;

    public Alumne() {
    }

    public Alumne(String nom, String cognoms, int edat, boolean sapjava) {
        this.nom = nom;
        this.cognoms = cognoms;
        this.edat = edat;
        this.sapjava = sapjava;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getCognoms() {
        return cognoms;
    }

    public void setCognoms(String cognoms) {
        this.cognoms = cognoms;
    }

    public int getEdat() {
        return edat;
    }

    public void setEdat(int edat) {
        this.edat = edat;
    }

    public boolean isSapjava() {
        return sapjava;
    }

    public void setSapjava(boolean sapjava) {
        this.sapjava = sapjava;
    }

    @Override
    public String toString() {
        String sapjava = " ";
        sapjava = sapjava.concat((this.sapjava) ? "sap java" : "no en sap");
        return "Alumne{" +
                "nom='" + nom + '\'' +
                ", cognoms='" + cognoms + '\'' +
                ", edat=" + edat + sapjava +
                '}';
    }
}
