package dam.m3.uf5.poo;

public class ProvaAlumne {
    public static void main(String[] args) {
        Alumne a = new Alumne();
        a.setNom("Roger");
        a.setCognoms("Deu");
        a.setEdat(19);
        a.setSapjava(true);
        System.out.println(a);
        System.out.println(a.toString());

        AlumneDAMi ad = new AlumneDAMi("Roger", "Deu", 19, true, 5);
        System.out.println(ad);
        System.out.println(ad.getEdat());

    }
}
