package dam.m3.uf5.poo;

public class AlumneDAMi extends Alumne {
    private int notaia;

    public AlumneDAMi() {
    }

    public AlumneDAMi(String nom, String cognoms, int edat, boolean sapjava, int notaia) {
        super(nom, cognoms, edat, sapjava);
        this.notaia = notaia;
    }

    public int getNotaia() {
        return notaia;
    }

    public void setNotaia(int notaia) {
        this.notaia = notaia;
    }

    @Override
    public String toString() {
        return super.toString() + " " + this.getNotaia();
    }
}
