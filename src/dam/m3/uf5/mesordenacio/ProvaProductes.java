package dam.m3.uf5.mesordenacio;

import dam.m3.uf5.primerrepas.Persona;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;

public class ProvaProductes {
    public static void main(String[] args) {
        Producte p1 = new Producte("bb", 1, 5, false);
        Producte p2 = new Producte("a", 28, 1, true);
        Producte p3 = new Producte("ba", 5, 2, false);
        Producte p4 = new Producte("z", 12, 3, true);
        Producte p5 = new Producte("s", 5, 4, true);
        ArrayList<Producte> llista;
        llista = new ArrayList<Producte>(Arrays.asList(p1, p2, p3, p4, p5));

        Collections.sort(llista, new ComparatorPerPreu());   //Comparator per preu amb classe auxiliar
        System.out.println(llista);

        Collections.sort(llista, new ComparatorPerPreuDesc());   //Comparator per preu amb classe auxiliar desc
        System.out.println(llista);

        Collections.sort(llista, new ComparatorPerDisponibilitat());    //Comparator per disponibilitat amb classe auxiliar
        System.out.println(llista);

        Collections.sort(llista, new ComparatorPerDisponibilitatDesc());    //Comparator per disponibilitat amb classe auxiliar desc
        System.out.println(llista);

        Collections.sort(llista, new ComparatorPerPreuDisponibilitat());    //Comparator per preu i en igualtat per disponibilitat amb classe auxiliar
        System.out.println(llista);

        Collections.sort(llista, new ComparatorPerPreuDisponibilitatDesc());    //Comparator per preu i en igualtat per disponibilitat amb classe auxiliar desc
        System.out.println(llista);

        Collections.sort(llista, new Comparator<Producte>() {                //Comparator per preu classe interna asc
            @Override
            public int compare(Producte producte, Producte t1) {
                return (int) (producte.getPreus() - t1.getPreus());
            }
        });
        System.out.println(llista);

        Collections.sort(llista, new Comparator<Producte>() {                //Comparator per preu classe interna desc
            @Override
            public int compare(Producte producte, Producte t1) {
                return ((int) (producte.getPreus() - t1.getPreus())) * -1;
            }
        });
        System.out.println(llista);

        Collections.sort(llista, new Comparator<Producte>() {                //Comparator per disponibilitat classe interna asc
            @Override
            public int compare(Producte producte, Producte t1) {
                return Boolean.compare(producte.isDisponible(), t1.isDisponible());
            }
        });
        System.out.println(llista);

        Collections.sort(llista, new Comparator<Producte>() {                //Comparator per disponibilitat classe interna desc
            @Override
            public int compare(Producte producte, Producte t1) {
                return (Boolean.compare(producte.isDisponible(), t1.isDisponible())) * -1;
            }
        });
        System.out.println(llista);

        Collections.sort(llista, new Comparator<Producte>() {                //Comparator per preu i si coincideix, per disponibilitat classe interna asc
            @Override
            public int compare(Producte producte, Producte t1) {
                return Comparator.comparing(Producte::getPreus).thenComparing(Producte::isDisponible).compare(producte, t1);
            }
        });
        System.out.println(llista);

        Collections.sort(llista, (pr1, pr2) -> (int) (pr1.getPreus() - pr2.getPreus()));  //Comparator per preu amb lambda
        System.out.println(llista);

        Collections.sort(llista, (pr1, pr2) -> (int) (pr1.getPreus() - pr2.getPreus()) * -1);   //Comparator per preu desc amb lambda
        System.out.println(llista);

        Collections.sort(llista, (pr1, pr2) -> Boolean.compare(pr1.isDisponible(), pr2.isDisponible()));  //Comparator per disponibilitat amb lambda
        System.out.println(llista);

        Collections.sort(llista, (pr1, pr2) -> Boolean.compare(pr1.isDisponible(), pr2.isDisponible()) * -1);   //Comparator per disponibilitat desc amb lambda
        System.out.println(llista);

        Collections.sort(llista, (pr1, pr2) -> Comparator.comparing(Producte::getPreus).thenComparing(Producte::isDisponible).compare(pr1, pr2));  //Comparator per preu i en cas d'igualtat per descripcio amb lambda
        System.out.println(llista);

        llista.sort(Producte::compararPreuDisponibilitat);
        System.out.println(llista);
    }
}
