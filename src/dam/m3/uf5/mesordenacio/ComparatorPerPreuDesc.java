package dam.m3.uf5.mesordenacio;

import java.util.Comparator;

public class ComparatorPerPreuDesc implements Comparator<Producte> {
    @Override
    public int compare(Producte producte, Producte t1) {
        return ((int) (producte.getPreus() - t1.getPreus())) * -1;
    }
}
