package dam.m3.uf5.mesordenacio;

import java.util.Comparator;

public class ComparatorPerPreuDisponibilitat implements Comparator<Producte> {
    @Override
    public int compare(Producte producte, Producte t1) {
        return Comparator.comparing(Producte::getPreus).thenComparing(Producte::isDisponible).compare(producte, t1);
    }
}
