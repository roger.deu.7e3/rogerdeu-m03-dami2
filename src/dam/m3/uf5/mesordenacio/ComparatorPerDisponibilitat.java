package dam.m3.uf5.mesordenacio;

import java.util.Comparator;

class ComparatorPerDisponibilitat implements Comparator<Producte> {

    @Override
    public int compare(Producte producte, Producte t1) {
        return Boolean.compare(producte.isDisponible(), t1.isDisponible());
    }
}
