package dam.m3.uf5.mesordenacio;

import java.util.Comparator;

public class Producte {
    private String descripcio;
    private double preus;
    private int codi;
    private boolean disponible;

    public Producte(String descripcio, double preus, int codi, boolean disponible) {
        this.descripcio = descripcio;
        this.preus = preus;
        this.codi = codi;
        this.disponible = disponible;
    }

    public Producte() {
    }

    @Override
    public String toString() {
        String disponibilitat = "";
        disponibilitat = disponibilitat.concat(this.disponible ? "està disponible" : "no està disponible");
        return "Producte{" +
                "descripcio='" + descripcio + '\'' +
                ", preus=" + preus +
                ", codi=" + codi +
                ", disponible=" + disponibilitat +
                '}';
    }

    public String getDescripcio() {
        return descripcio;
    }

    public void setDescripcio(String descripcio) {
        this.descripcio = descripcio;
    }

    public double getPreus() {
        return preus;
    }

    public void setPreus(double preus) {
        this.preus = preus;
    }

    public int getCodi() {
        return codi;
    }

    public void setCodi(int codi) {
        this.codi = codi;
    }

    public boolean isDisponible() {
        return disponible;
    }

    public void setDisponible(boolean disponible) {
        this.disponible = disponible;
    }

    public static int compararPreuDisponibilitat(Producte p1, Producte p2) {
        return Comparator.comparing(Producte::getPreus).thenComparing(Producte::isDisponible).compare(p1, p2);
    }
}
