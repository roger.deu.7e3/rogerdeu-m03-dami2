package dam.m3.uf5.repascollections;

import java.io.File;
import java.text.NumberFormat;
import java.util.*;

public class Ex1 {
    public static void main(String[] args) {
        TreeMap<Integer,Double> map;
        map = omplirMapa();
        map = ordenarMapa(map);
        System.out.println(map.firstEntry());
    }

    public static TreeMap<Integer,Double> omplirMapa() {
        File f = new File("telefons.txt");
        TreeMap<Integer, Double> map = new TreeMap<>();
        Locale spanish = new Locale("es", "ES");
        NumberFormat nf = NumberFormat.getInstance(spanish);
        String integro = "", doub = "";
        int contador = 0;
        try {
            Scanner lector = new Scanner(f);
            while (lector.hasNext()) {
                if (contador == 0) {
                    integro = lector.next();
                    contador++;
                }
                if (contador == 1) {
                    contador = 0;
                    doub = lector.next();

                    int key = Integer.parseInt(integro);
                    double value = nf.parse(doub).doubleValue();

                    if (map.containsKey(key)) {
                        double numero = map.get(key);
                        value += numero;
                    }
                    map.put(key, value);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return map;
    }

    public static TreeMap<Integer,Double> ordenarMapa(TreeMap<Integer,Double> map) {
        final TreeMap<Integer, Double> mapSortedByValues;
        mapSortedByValues = new TreeMap<>(new Comparator<Integer>() {
            public int compare(Integer o1, Integer o2) {
                return (map.get(o1).compareTo(map.get(o2))) * -1;
            }
        });
        mapSortedByValues.putAll(map);
        return mapSortedByValues;
    }

}
