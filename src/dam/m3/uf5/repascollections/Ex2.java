package dam.m3.uf5.repascollections;

import java.util.*;

public class Ex2 {
    public static void main(String[] args) {
        int counterWords=1;
        TreeMap<String,Paraula> map = new TreeMap<String,Paraula>();
        menu(map);
    }

    public static void menu(TreeMap<String,Paraula> map) {
        Scanner lector = new Scanner(System.in);
        int contador = 0, opcio=0;
        boolean marca=false;
        System.out.println("Tria l'opció: \n[1] Afegir Paraula\n[2] Consultar Paraula\n[3] Ordenar paraules ascendentment" +
                "\n[4] Ordenar paraules descendentment\n[5] Consultar sinonims\n[6] Consultar per lletra i longitud\n[0] Per acabar");
        while (!marca) {
            while (!lector.hasNextInt()){
                System.out.println("Aixo no es un numero");
                lector.nextLine();
                menu(map);
            }
            opcio = lector.nextInt();
            if (opcio >= 0 && opcio <= 6) {
                marca = true;
            }
        }
        switch (opcio) {
            case 0:
                break;
            case 1:
                addWord(map,contador);
                break;
            case 2:
                consultAWord(map);
                break;
            case 3:
                ordenarParaulesAsc(map);
                break;
            case 4:
                ordenarParaulesDesc(map);
                break;
            case 5:
                consultarSinonims(map);
                break;
            case 6:
                consultarLletraLongitud(map);
                break;
        }
        if (opcio != 0) {
            menu(map);
        }
    }

    private static void consultarLletraLongitud(TreeMap<String, Paraula> map) {
        System.out.println("Introdueix una lletra amb la que buscar la paraula");
        String paraula=""; int longitud = 0;
        Scanner lector = new Scanner(System.in);
        if (lector.hasNext()){
            paraula = lector.next();
        }
        lector.nextLine();
        System.out.println("Ara la seva logitud");
        if (lector.hasNextInt()){
            longitud = lector.nextInt();
        }
        Collection<Paraula> values = map.values();
        for (Paraula p : values) {
            if (p.getNom().startsWith(paraula) && p.getLongitud() == longitud) {
                System.out.println(p);
            }
        }
    }

    private static void consultarSinonims(TreeMap<String, Paraula> map) {
        String sinonim;
        Collection<Paraula> valors = map.values();
        for (Paraula p : valors) {
            sinonim = p.getSignificat();
            for (Paraula p2 : valors) {
                if (p2.getSignificat().equalsIgnoreCase(sinonim) && p2.getNom()!=p.getNom()) {
                    System.out.println(p.getNom() + " es sinonim de: " + p2.getNom());
                }
            }
        }
    }


    private static void ordenarParaulesDesc(TreeMap<String, Paraula> map) {
        TreeMap<String,Paraula> mapaOrdenat;
        mapaOrdenat = new TreeMap<String, Paraula>(new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return (s.compareToIgnoreCase(t1))*-1;
            }
        });
        mapaOrdenat.putAll(map);
        Collection<Paraula> values = mapaOrdenat.values();
        for (Paraula mapIter :values) {
            System.out.println(mapIter);
        }
    }

    private static void ordenarParaulesAsc(TreeMap<String, Paraula> map) {
        TreeMap<String,Paraula> mapaOrdenat;
        mapaOrdenat = new TreeMap<String, Paraula>(new Comparator<String>() {
            @Override
            public int compare(String s, String t1) {
                return s.compareToIgnoreCase(t1);
            }
        });
        mapaOrdenat.putAll(map);
        Collection<Paraula> values = mapaOrdenat.values();
        for (Paraula mapIter :values) {
            System.out.println(mapIter);
        }
    }

    public static void consultAWord(TreeMap<String,Paraula> map) {
        String paraula="";
        Scanner lector = new Scanner(System.in);
        System.out.println("Introdueix la paraula a buscar");
        if (lector.hasNext()){
            paraula = lector.next();
        }
        Collection<Paraula> values = map.values();
        for (Paraula p: values) {
            if (p.getNom().equalsIgnoreCase(paraula)) {
                System.out.println(p);
            }
        }
    }

    public static Paraula addNewWord() {
        String nom="",significat="";
        int lognitud;
        Scanner lector = new Scanner(System.in);
        System.out.println("Introdueix la paraula");
        if (lector.hasNext()){
            nom = lector.next();
        }
        lector.nextLine();
        System.out.println("Introdueix el seu significat");
        if (lector.hasNext()){
            significat = lector.nextLine();
        }
        lognitud = nom.length();
        return new Paraula(nom,significat,lognitud);
    }

    public static void addWord(TreeMap<String, Paraula> map, int contador) {
        Paraula p = addNewWord();
        map.put(p.getNom(),p);
    }

}