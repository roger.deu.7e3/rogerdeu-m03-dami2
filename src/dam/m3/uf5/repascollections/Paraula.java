package dam.m3.uf5.repascollections;

public class Paraula {
    private String nom, significat;
    private int longitud;

    public Paraula(String nom, String significat, int longitud) {
        this.nom = nom;
        this.significat = significat;
        this.longitud = longitud;
    }

    @Override
    public String toString() {
        return "Paraula{" +
                "nom='" + nom + '\'' +
                ", significat='" + significat + '\'' +
                ", longitud=" + longitud +
                '}';
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getSignificat() {
        return significat;
    }

    public void setSignificat(String significat) {
        this.significat = significat;
    }

    public int getLongitud() {
        return longitud;
    }

    public void setLongitud(int longitud) {
        this.longitud = longitud;
    }
}
